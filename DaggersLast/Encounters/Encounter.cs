﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DaggersLast
{
    public class Encounter
    {
        public static int EncounterWidth = 320;
        public static int EncounterHeight = 160;
        public string Type;
        private Rectangle destRect;
        private Action exit;
        private Texture2D PrimaryTexture;
        private int currentTexture = 0;
        public Encounter(string type, Texture2D primaryTexture, Action exitEncounter)
        {
            Type = type;
            PrimaryTexture = primaryTexture;
            exit = exitEncounter;
            destRect = new Rectangle(0, 0, EncounterWidth, EncounterHeight);
        }
        public void Update(GameTime gameTime, Input input)
        {
            if (input.Action2Pressed && !input.WasAction2Pressed)
            {
                exit();
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            // draw the background
            spriteBatch.Draw(
                PrimaryTexture,
                destRect,
                Color.White);
            // draw the animated sprite
            // draw the text
            // draw the encounter action options
        }
    }
}
