﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DaggersLast
{
    public class Character
    {
        // these never change, the char always stays centered on screen and the same size
        public readonly int HitWidth = 16;
        public readonly int HitHeight = 20;
        public readonly Vector2 HitPosition = new Vector2(153, 97);

        private const string Idle = "idle";
        private const string Run = "run";
        private const string Jump = "jump";
        private const string Fall = "fall";

        // Constants for controlling horizontal movement
        private const float BaseRunSpeed = 0.75f;
        private const float MoveAcceleration = 13000.0f;
        private const float MaxMoveSpeed = 1000.0f;// 1750.0f;
        private const float GroundDragFactor = 0.38f;
        private const float AirDragFactor = 0.48f;
        private const float MoveStickScale = 1.0f;

        // Constants for controlling vertical movement
        private const float MaxJumpTime = 0.3f;
        private const float JumpLaunchVelocity = -2400.0f;
        private const float GravityAcceleration = 2000.0f;//2800//3400.0f;
        private const float MaxFallSpeed = 370.0f;// 550.0f;
        private const float JumpControlPower = 0.08f;
        private int fallTimer = 0;

        // animation
        private readonly TimeSpan idleFrameRate = TimeSpan.FromSeconds(5f / 30f);
        private readonly TimeSpan runFrameRate = TimeSpan.FromSeconds(2.5f / 30f);
        private TimeSpan currentFrameRate;
        private Rectangle charDrawBounds = new Rectangle(0, 0, 1, 1);
        private TimeSpan previousFrameChangeTime = TimeSpan.Zero;
        private int currentFrame = 0;
        private List<Rectangle> charAnimation;
        private bool facingRight = true;
        private string previousAnimation;
        private string currentAnimation;
        private float movement;
        // Jumping state
        private bool isOnGround;
        private bool isJumping;
        private bool wasJumping;
        private float jumpTime;
        // temp char hit state
        private Rectangle charHitDestRect;
        Texture2D charHit;

        // Physics state
        // these update to track the char global position in the entire zone
        // these controll the render position of the zone tiles
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        Vector2 position = new Vector2(0, 0);

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        Vector2 velocity;

        public Character(SpriteBatch spriteBatch, Vector2 startingPosition)
        {
            // white hit box for testing
            charHit = new Texture2D(spriteBatch.GraphicsDevice, HitWidth, HitHeight);
            Color[] data = new Color[HitWidth * HitHeight];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.White;
            charHit.SetData(data);
            charHitDestRect = new Rectangle(
                (int)HitPosition.X,
                (int)HitPosition.Y,
                HitWidth,
                HitHeight);
            // initial animation state
            charAnimation = CharacterFrames.CharacterIdleRight;
            previousAnimation = Idle;
            currentAnimation = Idle;
            // set starting position
            position = startingPosition;
        }

        private void HandleCollisions(List<ZoneTileLayout> layouts)
        {
            isOnGround = false;
            // translate character position to zone tile position
            Rectangle charBounds = new Rectangle((int)Position.X, (int)Position.Y, HitWidth, HitHeight);
            // loop through each layout
            for (int i = 0; i < 2; ++i)
            {
                int numberOfPlatforms = layouts[i].Platforms.Count;
                // loop through each platform(CollisionRectangle) in each layout
                for (int j = 0; j < numberOfPlatforms; ++j)
                {
                    CollisionRectangle collRect = layouts[i].Platforms[j];
                    Rectangle intersect = Rectangle.Intersect(charBounds, collRect.Rect);
                    // the walls (horizontal hits)
                    if (intersect.Width < intersect.Height)
                    {
                        if (intersect.Width > 0 && intersect.X == collRect.Rect.X)
                        {
                            // hit something to the right
                            Position = new Vector2(collRect.Rect.X - HitWidth, Position.Y);
                        }
                        else if (intersect.Width > 0 && intersect.X == charBounds.X)
                        {
                            // hit something to the left
                            Position = new Vector2(collRect.Rect.X + collRect.Rect.Width, Position.Y);
                        }
                    }
                    // the ground (vertical hits)
                    if (intersect.Height < intersect.Width)
                    // if (charBounds.Y + charBounds.Height)
                    {
                        if (intersect.Height > 0 && intersect.Y == collRect.Rect.Y)
                        {
                            // hit the ground
                            isOnGround = true;
                            Position = new Vector2(Position.X, collRect.Rect.Y - HitHeight);
                        }
                        else if (intersect.Height > 0 && intersect.Y == charBounds.Y)
                        {
                            // hit the ceiling
                            jumpTime = MaxJumpTime - 0.06f;
                            Position = new Vector2(Position.X, collRect.Rect.Y + HitHeight);
                        }
                    }
                }
            }
        }

        public void ApplyPhysics(GameTime gameTime, List<ZoneTileLayout> layouts, int zoneTiles)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 previousPosition = Position;

            // Base velocity is a combination of horizontal movement control and
            // acceleration downward due to gravity.
            velocity.X += movement * MoveAcceleration * elapsed;
            velocity.Y = MathHelper.Clamp(velocity.Y + GravityAcceleration * elapsed, -MaxFallSpeed, MaxFallSpeed);
            
            // check for jumping
            velocity.Y = DoJump(velocity.Y, gameTime);

            // Apply pseudo-drag horizontally.
            if (isOnGround)
                velocity.X *= GroundDragFactor;
            else
                velocity.X *= AirDragFactor;

            // Prevent the player from running faster than his top speed.            
            velocity.X = MathHelper.Clamp(velocity.X, -MaxMoveSpeed, MaxMoveSpeed);

            // Apply velocity.
            Position += velocity * elapsed;
            float minPositionX = Game1.ZoneTileWidth;
            float maxPositionX = (zoneTiles - 1) * Game1.ZoneTileWidth - HitWidth;
            float finalPositionX = (float)Math.Clamp(Math.Round(Position.X), minPositionX, maxPositionX);
            Position = new Vector2(finalPositionX, (float)Math.Round(Position.Y));

            // Check player against collision rects
            HandleCollisions(layouts);

            // If the collision stopped us from moving, reset the velocity to zero.
            if (Position.X == previousPosition.X)
                velocity.X = 0;

            if (Position.Y == previousPosition.Y)
            {
                velocity.Y = 0;
                fallTimer = 0;
            }
        }

        private float DoJump(float velocityY, GameTime gameTime)
        {
            // If the player wants to jump
            if (isJumping)
            {
                currentFrameRate = idleFrameRate;
                // Begin or continue a jump
                if ((!wasJumping && isOnGround) || jumpTime > 0.0f)
                {
                    jumpTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (facingRight)
                    {
                        charAnimation = CharacterFrames.CharacterJumpRight;
                    }
                    else
                    {
                        charAnimation = CharacterFrames.CharacterJumpLeft;
                    }
                    currentAnimation = Jump;
                }

                // If we are in the ascent of the jump
                if (0.0f < jumpTime && jumpTime <= MaxJumpTime)
                {
                    // Fully override the vertical velocity with a power curve that gives players more control over the top of the jump
                    velocityY = JumpLaunchVelocity * (1.0f - (float)Math.Pow(jumpTime / MaxJumpTime, JumpControlPower));
                }
                else
                {
                    // Reached the apex of the jump
                    jumpTime = 0.0f;
                }
            }
            else
            {
                // Continues not jumping or cancels a jump in progress
                jumpTime = 0.0f;
            }
            wasJumping = isJumping;

            return velocityY;
        }

        private void GetInput(Input input)
        {
            // Ignore small movements to prevent running in place.
            if (Math.Abs(movement) < 0.5f) movement = 0.0f;

            // left movement
            if (!input.RightPressed && input.LeftPressed)
            {
                // adjust hit state position when changing directions for smoother flip aanimation
                if (facingRight) Position = new Vector2(Position.X + 1, Position.Y);
                // set new direction
                facingRight = false;
                movement = -BaseRunSpeed;
            }

            // right movement
            if (!input.LeftPressed && input.RightPressed)
            {
                // adjust hit state position when changing directions for smoother flip aanimation
                if (!facingRight) Position = new Vector2(Position.X - 1, Position.Y);
                // set new direction
                facingRight = true;
                movement = BaseRunSpeed;
            }

            // jumping
            isJumping = input.Action1Pressed ||
                input.KeyState.IsKeyDown(Keys.W) ||
                input.KeyState.IsKeyDown(Keys.Up);
        }

        private void checkIdleRunFall()
        {
            // check for running or idle
            if (isOnGround && Math.Abs(velocity.Y) == 0)
            {
                if (Math.Abs(Velocity.X) > 0)
                {
                    currentFrameRate = runFrameRate;
                    if (facingRight)
                    {
                        charAnimation = CharacterFrames.CharacterRunRight;
                    }
                    else
                    {
                        charAnimation = CharacterFrames.CharacterRunLeft;
                    }
                    currentAnimation = Run;
                }
                else
                {
                    if (currentAnimation == Run && currentFrame > 2 && currentFrame < 6)
                    {
                        // don't switch to the idle state yet, keep it in running animation in order to backstep through
                        // the final run frames to get to a point to smoothly transition to the idle animation
                    }
                    else
                    {
                        currentFrameRate = idleFrameRate;
                        if (facingRight)
                        {
                            charAnimation = CharacterFrames.CharacterIdleRight;
                        }
                        else
                        {
                            charAnimation = CharacterFrames.CharacterIdleLeft;
                        }
                        currentAnimation = Idle;
                    }
                }
            }
            // fall state
            if (!isOnGround && !isJumping)
            {
                currentFrameRate = idleFrameRate;
                if (fallTimer > 8)
                {
                    // only switch to fall graphic after an 8 frame fall time
                    if (facingRight)
                    {
                        charAnimation = CharacterFrames.CharacterFallRight;
                    }
                    else
                    {
                        charAnimation = CharacterFrames.CharacterFallLeft;
                    }
                    currentAnimation = Fall;
                }
                else if (currentAnimation == Jump)
                {
                    // handle direction switching during falling
                    if (facingRight)
                    {
                        charAnimation = CharacterFrames.CharacterJumpRight;
                    }
                    else
                    {
                        charAnimation = CharacterFrames.CharacterJumpLeft;
                    }
                }
                fallTimer++;
            }
        }

        private void handleFrameOverrides()
        {
            // if the character was idle and started running, start the run from the first run frame
            if (previousAnimation == Idle && currentAnimation == Run)
            {
                currentFrame = 0;
            }
            // if the character was running and stopped, gracefully transition to idle state
            if (previousAnimation == Run && currentAnimation == Idle)
            {
                if (currentFrame == 3 || currentFrame == 5) currentFrame = 2;
                if (currentFrame == 4) currentFrame = 3;
            }

            // if the character was not jumping and jumped, start from the first jump frame
            if (previousAnimation != Jump && currentAnimation == Jump)
            {
                currentFrame = 0;
            }
            // if the character was running and jumped, gracefully transition to correct starting jump frame
            /*if (previousAnimation == Run && currentAnimation == Jump)
            {
                if (currentFrame == 3 || currentFrame == 5) currentFrame = 2;
                if (currentFrame == 4) currentFrame = 3;
                if (currentFrame == 6) currentFrame = 1;
                if (currentFrame == 7) currentFrame = 0;
            }*/
            // if the character was doing anything and falls, make sure it starts on an existing fall frame
            if (currentAnimation == Fall)
            {
                currentFrame = 0;
            }
        }
        public void Update(
            GameTime gameTime,
            Input input,
            List<ZoneTileLayout> layouts,
            int zoneTiles)
        {
            // track the time difference between the last drawn frame
            var nowTime = gameTime.TotalGameTime;
            var dtFrame = nowTime - previousFrameChangeTime;

            // get controller input
            GetInput(input);
            // apply gravity
            ApplyPhysics(gameTime, layouts, zoneTiles);
            // check other animation states
            checkIdleRunFall();
            // check for final frame overrides
            handleFrameOverrides();

            // check if the newest current animation is different than the previous
            /*if (currentAnimation != previousAnimation)
            {
                // if it is, reset the time frame change to now, so the newest frame doesnt get cut short
                previousFrameChangeTime = gameTime.TotalGameTime;
                dtFrame = nowTime - previousFrameChangeTime;
            }*/

            // after any overrides, save the latest animation as the previous, since it wont be updated again until the next draw
            previousAnimation = currentAnimation;

            if (dtFrame >= currentFrameRate)
            {
                // update the frame of the current animation based on framerate
                previousFrameChangeTime = nowTime;
                currentFrame++;
            }

            // ensure the frame actually exists in the animation
            if (currentFrame >= charAnimation.Count)
            {
                if (currentAnimation == Jump)
                {
                    currentFrame = 2;
                }
                else
                {
                    currentFrame = 0;
                }
            }

            // Clear input.
            movement = 0.0f;
            isJumping = false;

            // adjust x per facing direction
            int flipAdjust = facingRight ? 10 : 13;
            int finalXPos = (int)HitPosition.X - flipAdjust;
            // adjust y 
            int finalYPos = (int)HitPosition.Y - (charAnimation[currentFrame].Height - HitHeight - 1);
            // set final char position for draw
            charDrawBounds.X = finalXPos;
            charDrawBounds.Y = finalYPos;
            charDrawBounds.Width = charAnimation[currentFrame].Width;
            charDrawBounds.Height = charAnimation[currentFrame].Height;
        }

        public void Draw(
            SpriteBatch spriteBatch,
            Texture2D charTexture)
        {
            // draw hit box for testing
            /*spriteBatch.Draw(
                texture: charHit,
                destinationRectangle: charHitDestRect,
                color: Color.Red
            );*/
            // char graphics
            spriteBatch.Draw(
                texture: charTexture,
                destinationRectangle: charDrawBounds,
                sourceRectangle: charAnimation[currentFrame],
                color: Color.White
            );
        }
    }
}
