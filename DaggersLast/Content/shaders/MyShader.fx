﻿sampler inputTexture;
int pixelation;

float4 MainPS(float2 originalUV: TEXCOORD0) : COLOR0
{
	originalUV *= float2(1280, 720);

	float2 newUV;
	// newUV.x = round(originalUV.x / pixelation) * pixelation;
	newUV.x = round(originalUV.x - (pixelation * 0.5)) + (originalUV.x % 12);
	// newUV.y = round(originalUV.y / pixelation) * pixelation;
	newUV.y = round(originalUV.y - (pixelation * 0.5)) + (originalUV.y % 9);

	// again: change this to match your screen's resolution
	newUV /= float2(1280, 720);

	return tex2D(inputTexture, newUV);
}

technique Techninque1
{
	pass Pass1
	{
		PixelShader = compile ps_3_0 MainPS();
		AlphaBlendEnable = TRUE;
		DestBlend = INVSRCALPHA;
		SrcBlend = SRCALPHA;
	}
};
/*
#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

matrix WorldViewProjection;

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
};

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = mul(input.Position, WorldViewProjection);
	output.Color = input.Color;

	return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	return input.Color;
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
*/