﻿using Microsoft.Xna.Framework.Input;

namespace DaggersLast
{
    public class Input
    {
        // natural states
        public KeyboardState KeyState;
        public GamePadState PadState;
        // current states
        public bool LeftPressed;
        public bool RightPressed;
        public bool UpPressed;
        public bool DownPressed;
        public bool Action1Pressed;
        public bool Action2Pressed;
        public bool Context1Pressed;
        public bool Context2Pressed;
        public bool MenuPressed;
        // previous states
        public bool WasLeftPressed;
        public bool WasRightPressed;
        public bool WasUpPressed;
        public bool WasDownPressed;
        public bool WasAction1Pressed;
        public bool WasAction2Pressed;
        public bool WasContext1Pressed;
        public bool WasContext2Pressed;
        public bool WasMenuPressed;

        public void Update(KeyboardState keyboardState, GamePadState gamePadState)
        {
            // save natural state references
            KeyState = keyboardState;
            PadState = gamePadState;

            // save all previous states
            WasLeftPressed = LeftPressed;
            WasRightPressed = RightPressed;
            WasUpPressed = UpPressed;
            WasDownPressed = DownPressed;
            WasAction1Pressed = Action1Pressed;
            WasAction2Pressed = Action2Pressed;
            WasContext1Pressed = Context1Pressed;
            WasContext2Pressed = Context2Pressed;
            WasMenuPressed = MenuPressed;

            // assign all current states
            // left
            LeftPressed = keyboardState.IsKeyDown(Keys.A) ||
                keyboardState.IsKeyDown(Keys.Left) ||
                gamePadState.IsButtonDown(Buttons.DPadLeft) ||
                gamePadState.ThumbSticks.Left.X < 0;
            // right
            RightPressed = keyboardState.IsKeyDown(Keys.D) ||
                keyboardState.IsKeyDown(Keys.Right) ||
                gamePadState.IsButtonDown(Buttons.DPadRight) ||
                gamePadState.ThumbSticks.Left.X > 0;
            // up
            UpPressed = keyboardState.IsKeyDown(Keys.W) ||
                keyboardState.IsKeyDown(Keys.Up) ||
                gamePadState.IsButtonDown(Buttons.DPadUp) ||
                gamePadState.ThumbSticks.Left.Y > 0;
            // down
            DownPressed = keyboardState.IsKeyDown(Keys.S) ||
                keyboardState.IsKeyDown(Keys.Down) ||
                gamePadState.IsButtonDown(Buttons.DPadDown) ||
                gamePadState.ThumbSticks.Left.Y < 0;
            // primary action: jump or select
            Action1Pressed = gamePadState.IsButtonDown(Buttons.A) ||
                gamePadState.IsButtonDown(Buttons.B) ||
                keyboardState.IsKeyDown(Keys.RightControl);
            // secondary action: select
            Action2Pressed = gamePadState.IsButtonDown(Buttons.X) ||
                gamePadState.IsButtonDown(Buttons.Y) ||
                keyboardState.IsKeyDown(Keys.Enter);
            // context 1: character screen
            Context1Pressed = gamePadState.IsButtonDown(Buttons.LeftShoulder) ||
                keyboardState.IsKeyDown(Keys.C);
            // context 2: inventory screen
            Context1Pressed = gamePadState.IsButtonDown(Buttons.RightShoulder) ||
                keyboardState.IsKeyDown(Keys.I);
            // menu
            MenuPressed = gamePadState.IsButtonDown(Buttons.Back) ||
                keyboardState.IsKeyDown(Keys.Escape);
        }
    }
}
