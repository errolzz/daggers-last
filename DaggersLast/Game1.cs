﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Input;

namespace DaggersLast
{
    public class Game1 : Game
    {
        public static int GameWidth = 320;
        public static int GameHeight = 180;
        public static int GameScale = 4;
        public static int ZoneTileWidth = 320;
        public static int ZoneTileHeight = 360;
        private Vector3 screenScalingFactor;
        private Matrix globalTransformation;
        private Texture2D characterTexture;
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private GamePadState gamePadState;
        private KeyboardState keyboardState;
        private GameBar gameBar;
        private Character character;
        private Zone zone;
        private Input input;
        // TODO: how to decided zone size?
        private int zoneSize = 12; // this includes end caps

        public Encounter ActiveEncounter;
        public string Overlay; // "None", "Character", "Inventory"

        public Game1()
        {
            Content.RootDirectory = "Content";
            graphics = new GraphicsDeviceManager(this);
            IsMouseVisible = false;
        }

        protected override void Initialize()
        {
            // create input manager
            input = new Input();
            // set screen size
            // TODO: switch gameScale to be floor(screen size / zoneTileWidth)
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            // optimize frame rate
            graphics.SynchronizeWithVerticalRetrace = false; //Vsync
            IsFixedTimeStep = true;
            // TargetElapsedTime = TimeSpan.FromSeconds(1d / 30d);
            graphics.ApplyChanges();
            // sprite batch
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // generate zone tile collision rectangles
            ZoneTileData.Generate();
            // generate sprite frames
            CharacterFrames.Generate();
            screenScalingFactor = new Vector3(GameScale, GameScale, 1);
            globalTransformation = Matrix.CreateScale(screenScalingFactor);
            // init
            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            // starting zone
            Color bgColor = new Color(10, 84, 82); // TODO: where does this get set
            int zoneSize = 12; // TODO: where does this get set? // this includes end caps
            // TODO: replace this with a component
            Texture2D testGameBar = Content.Load<Texture2D>("imgs/tests/gamebar");
            gameBar = new GameBar(testGameBar);
            // create the zone
            zone = new Zone(
                Content,
                spriteBatch,
                activateNode,
                Zone.Forest,
                bgColor,
                zoneSize);
            // character sprites
            characterTexture = Content.Load<Texture2D>("imgs/char");
            // create the character, position them on the second zone tile layout
            character = new Character(
                spriteBatch,
                new Vector2(zone.Layouts[1].StartingPositionX + ZoneTileWidth, zone.Layouts[1].StartingPositionY));
        }
        
        protected override void Update(GameTime gameTime)
        {
            // single keyboard and gamepad states
            keyboardState = Keyboard.GetState();
            gamePadState = GamePad.GetState(PlayerIndex.One);
            // update input manager
            input.Update(keyboardState, gamePadState);

            // TODO: change this to open game menu
            if (input.MenuPressed) Exit();

            // TODO: create better logic on what needs updating based on game state
            if (ActiveEncounter == null)
            {
                // update the zone
                zone.Update(gameTime, input, character);

                // update the character
                character.Update(gameTime, input, zone.VisibleZoneLayouts, zoneSize);
            }
            else if (ActiveEncounter != null)
            {
                ActiveEncounter.Update(gameTime, input);
            }
            
            // update game time
            base.Update(gameTime);
        }

        private void activateNode(Node node)
        {
            // create a new encounter based on node type
            if (node.Type == Node.Enemy)
            {
                ActiveEncounter = new Encounter(node.Type, zone.EncounterTexture, exitEncounter);
            }
        }

        private void exitEncounter()
        {
            ActiveEncounter = null;
        }

        // TODO: maybe change this to DrawText, and have it run drawText functions in each component...
        private void DrawUI()
        {
            // possibly use a different spriteBatch.Begin / End here
            gameBar.Draw(spriteBatch);
        }
        

        protected override void Draw(GameTime gameTime)
        {
            // TODO: have the current zone type set this color
            GraphicsDevice.Clear(zone.BackgroundColor);
            // start the draw
            spriteBatch.Begin(samplerState: SamplerState.PointClamp, transformMatrix: globalTransformation);
            // TODO: create and use app state to decide to draw zone, encounter, or overlay UI
            if (ActiveEncounter == null)
            {
                // draw the zone background
                zone.DrawBackground(spriteBatch);
                // draw the middleground
                zone.DrawMiddleground(spriteBatch);
                // draw the platforms
                zone.DrawPlatforms(spriteBatch);
                // draw the character
                character.Draw(spriteBatch, characterTexture);
                // draw the foreground
                zone.DrawForeground(spriteBatch);
                // draw the nodes
                zone.DrawNodes(spriteBatch);
            }
            if (ActiveEncounter != null)
            {
                ActiveEncounter.Draw(spriteBatch);
            }
            // draw the UI
            DrawUI();
            // end the draw
            spriteBatch.End();
            // draw!
            base.Draw(gameTime);
        }
    }
}
