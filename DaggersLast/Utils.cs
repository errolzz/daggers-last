﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace DaggersLast
{
    static class Utils
    {
        public static List<Rectangle> CreateFrames(int frames, int frameSize, int rowY, bool looping)
        {
            // get total frames in animation
            int totalFrames = frames;
            if (looping) totalFrames += (frames - 2);
            // create a rectangle for each frame
            List<Rectangle> rects = new List<Rectangle>();
            for (int i = 0; i < totalFrames; i++)
            {
                int rx = i < frames ? i * frameSize : ((frames - 1) - ((i + 1) - frames)) * frameSize;
                Rectangle r = new Rectangle(x: rx, y: rowY, width: frameSize, height: frameSize);
                rects.Add(r);
            }
            return rects;
        }

        // save pixel colors to grid
        // public Color[,] Pixels;
        /*
        Color[] rawData = new Color[AllTextures[0].Width * AllTextures[0].Height];
        AllTextures[0].GetData<Color>(rawData);
        Pixels = new Color[AllTextures[0].Height, AllTextures[0].Width];
        for (int row = 0; row < AllTextures[0].Height; row++)
        {
            for (int column = 0; column < AllTextures[0].Width; column++)
            {
                // Assumes row major ordering of the array.
                Pixels[row, column] = rawData[row * AllTextures[0].Width + column];
            }
        }
        Color color = zone.Pixels[70, 10]; // 10th pixel on the 7th row
        */
    }
}
