﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DaggersLast
{
    public class GameBar
    {
        public static int Width = Game1.GameWidth;
        public static int Height = 20;
        private Texture2D graphic;
        private Rectangle destRect;
        public GameBar(Texture2D testGraphic)
        {
            graphic = testGraphic;
            destRect = new Rectangle(0, Game1.GameHeight - Height, Width, Height);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: graphic,
                destinationRectangle: destRect,
                color: Color.White
            );
        }
    }
}
