﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DaggersLast
{
    public class AnimatedSprite
    {
        public readonly int X = 0;
        public readonly int Y = 0;
        public readonly Texture2D SpriteTexture;
        public readonly List<Rectangle> Frames;
        private readonly TimeSpan frameRate = TimeSpan.FromSeconds(5f / 30f);
        private Rectangle drawBounds = new Rectangle(0, 0, 1, 1);
        private TimeSpan previousFrameChangeTime = TimeSpan.Zero;
        private int currentFrame = 0;
        public AnimatedSprite(Texture2D spriteTexture, List<Rectangle> frames, int x, int y)
        {
            SpriteTexture = spriteTexture;
            Frames = frames;
            X = x;
            Y = y;

            // set final size for draw
            drawBounds.Width = Frames[0].Width;
            drawBounds.Height = Frames[0].Height;
        }
        public void Update(GameTime gameTime, int parentX, int parentY)
        {
            // adjust x and y positions from global to local
            drawBounds.X = parentX + X;
            drawBounds.Y = parentY + Y;
            // handle framerate things
            var nowTime = gameTime.TotalGameTime;
            var dtFrame = nowTime - previousFrameChangeTime;

            if (dtFrame >= frameRate)
            {
                previousFrameChangeTime = nowTime;
                currentFrame++;
            }
            if (currentFrame >= Frames.Count)
            {
                currentFrame = 0;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                texture: SpriteTexture,
                destinationRectangle: drawBounds,
                sourceRectangle: Frames[currentFrame],
                color: Color.White
            );
        }
    }
}
