﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace DaggersLast
{
    static class CharacterFrames
    {
        public static List<Rectangle> CharacterIdleRight;
        public static List<Rectangle> CharacterRunRight;
        public static List<Rectangle> CharacterIdleLeft;
        public static List<Rectangle> CharacterRunLeft;
        public static List<Rectangle> CharacterJumpRight;
        public static List<Rectangle> CharacterJumpLeft;
        public static List<Rectangle> CharacterFallRight;
        public static List<Rectangle> CharacterFallLeft;
        public static void Generate()
        {
            CharacterIdleRight = Utils.CreateFrames(5, 40, 0, true);
            CharacterIdleLeft = Utils.CreateFrames(5, 40, 40, true);

            CharacterRunRight = Utils.CreateFrames(5, 40, 80, true);
            CharacterRunLeft = Utils.CreateFrames(5, 40, 120, true);

            CharacterJumpRight = Utils.CreateFrames(3, 40, 160, false);
            CharacterJumpLeft = Utils.CreateFrames(3, 40, 200, false);
            // use the last jump frames for the falls
            CharacterFallRight = new List<Rectangle>()
            {
                new Rectangle(80, 160, 40, 40),
            };
            CharacterFallLeft = new List<Rectangle>()
            {
                new Rectangle(80, 200, 40, 40),
            };
        }
    }
}