﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DaggersLast
{
    public class Node
    {
        public static string Collect = "Collect";
        public static string Enemy = "Enemy";
        public static string Environment = "Environment";
        public static string Dialogue = "Dialogue"; // in-town only?

        public Rectangle NodeRect;
        public Rectangle HitRect;
        public readonly string Type;
        private AnimatedSprite Sprite;
        private Texture2D NodeTexture;
        private Rectangle nodeHitDestRect;
        private bool Selected = false;

        public Node(Rectangle nodeRect, Rectangle hitRect, bool canBeBlocking, Texture2D nodeTexture)
        {
            HitRect = hitRect; // the global position in the entire zone
            NodeRect = nodeRect; // the local position in the tile layout
            Type = Enemy;
            NodeTexture = nodeTexture;
            // TODO: random check to decide node type (collect, enemy, etc) (use canBeBlocking too...)
            // might happen in Game1
            // TODO: pass a zone definition in here to determine how to generate the node content
            // TODO: create zone definitions that house zone-specific enemies and encounters
            // need static list of enemies/encounters by level for each zone (or zone type?)
            // TODO: create an AnimatedSprite that matches the node
        }

        public void Update(GameTime gameTime, int parentX, int parentY, bool hittingNode)
        {
            // also update the animated sprite
            // Sprite.Update(gameTime, parentX, parentY);
            // white hit box for testing (this gets replaced with an animated sprite
            nodeHitDestRect = new Rectangle(
                NodeRect.X + parentX,
                NodeRect.Y + parentY,
                HitRect.Width,
                HitRect.Height);
            // TODO: make this switch to the selected draw state (white outline)
            Selected = hittingNode;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            /*spriteBatch.Draw(
                texture: NodeTexture,
                destinationRectangle: nodeHitDestRect,
                color: Color.White
            );*/
        }
    }
}
