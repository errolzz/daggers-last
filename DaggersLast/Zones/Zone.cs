﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DaggersLast
{
    public class Zone
    {
        public static string Forest = "Forest";
        public static string Grassland = "Grassland";
        public static string Mountain = "Mountain";
        public static string Swamp = "Swamp";
        public static string Waste = "Waste";
        public static string Ruins = "Ruins";
        public static string Dungeon = "Dungeon";
        public static string Cave = "Cave";
        
        public readonly List<ZoneTileLayout> VisibleZoneLayouts = new List<ZoneTileLayout>();
        public readonly List<ZoneTileLayout> Layouts = new List<ZoneTileLayout>();
        public readonly List<CollisionRectangle> Nodes = new List<CollisionRectangle>(); //TODO: new Node class?
        public readonly Color BackgroundColor;
        private Action<Node> ActivateNode;
        private List<Texture2D> AllTextures = new List<Texture2D>();
        private List<ZoneTileLayout> AllLayouts = new List<ZoneTileLayout>();
        private List<Texture2D> Textures = new List<Texture2D>();
        private Texture2D UnderTexture;
        private Texture2D BackgroundTexture;
        private Texture2D MiddlegroundTexture;
        private Texture2D ForegroundTexture;
        private List<int> MiddleGroundItemPositions = new List<int>();
        private List<Texture2D> VisibleZoneTextures = new List<Texture2D>();
        private List<Rectangle> MiddlegroundItemSourceRects = new List<Rectangle>();
        private List<Rectangle> middlegroundItems;
        private Rectangle mainTileDestRect;
        private Rectangle neighborTileDestRect;
        private Rectangle mainBackgroundDestRect;
        private Rectangle neighborBackgroundDestRect;
        private Rectangle mainUnderDestRect;
        private Rectangle neighborUnderDestRect;
        private List<Rectangle> middlegroundDestRects = new List<Rectangle>();
        private SpriteBatch testSpriteBatch;
        // test textures
        public Texture2D EncounterTexture;
        // TODO: set up anything needed to track zone state (node statuses, anything else?)
        public Zone(
            ContentManager content,
            SpriteBatch spriteBatch,
            Action<Node> activateNode,
            string zoneType,
            Color backgroundColor,
            int zoneSize)
        {
            testSpriteBatch = spriteBatch;
            // set the background color
            BackgroundColor = backgroundColor;
            // save the reference to parent activate node
            ActivateNode = activateNode;

            // create fixed drawing rects
            mainTileDestRect = new Rectangle(0, 0, Game1.ZoneTileWidth, Game1.ZoneTileHeight);
            neighborTileDestRect = new Rectangle(0, 0, Game1.ZoneTileWidth, Game1.ZoneTileHeight);
            mainBackgroundDestRect = new Rectangle(0, 0, Game1.ZoneTileWidth, 200);
            neighborBackgroundDestRect = new Rectangle(0, 0, Game1.ZoneTileWidth, 200);
            mainUnderDestRect = new Rectangle(0, Game1.ZoneTileHeight, Game1.ZoneTileWidth, 40);
            neighborUnderDestRect = new Rectangle(0, Game1.ZoneTileHeight, Game1.ZoneTileWidth, 40);

            loadZone(content, zoneType);
            // TODO: load node textures for the zone type

            // select zone layouts, nodes, and textures
            buildZone(zoneSize);
            // build the middleground items
            buildMiddleground(zoneSize);
        }

        private void loadZone(ContentManager content, string zoneType)
        {
            // dictionary pairing zone layouts to textures
            Dictionary<ZoneTileLayout, string> texturesToLoad;
            // zone layouts and paths to textures for each zone type
            // TODO: move this dictionary to a static file
            Dictionary<ZoneTileLayout, string> forestTextures = new Dictionary<ZoneTileLayout, string>();
            forestTextures.Add(ZoneTileData.Tile5a, "imgs/forest-tile-5a");

            // TODO: create a static file for node textures for each zone type

            // use zoneType to get possible zone textures to load
            if (zoneType == Forest)
            {
                texturesToLoad = forestTextures;
                // load forest background, middleground, and foreground textures
                UnderTexture = content.Load<Texture2D>("imgs/forest-under");
                BackgroundTexture = content.Load<Texture2D>("imgs/forest-bg");
                MiddlegroundTexture = content.Load<Texture2D>("imgs/forest-mg");
                ForegroundTexture = content.Load<Texture2D>("imgs/forest-fg");
                // testing textures
                ForestForegroundFrames.Generate();
                middlegroundItems = new List<Rectangle>()
                {
                    new Rectangle(0, 0, 26, 270),
                    new Rectangle(26, 0, 43, 270),
                    new Rectangle(69, 0, 26, 270),
                    new Rectangle(95, 0, 58, 270),
                };
                // TODO: load forest node textures
                // load forest encounters
                EncounterTexture = content.Load<Texture2D>("imgs/tests/encounter1");
            }
            else
            {
                texturesToLoad = forestTextures;
            }

            // load textures and map them to appropriate ZoneTileLayouts
            foreach (KeyValuePair<ZoneTileLayout, string> kvp in texturesToLoad)
            {
                AllTextures.Add(content.Load<Texture2D>(kvp.Value));
                AllLayouts.Add(kvp.Key);
            }
        }

        private void buildZone(int zoneSize)
        {
            // randomly select layouts and textures
            for (int i = 0; i < zoneSize; ++i)
            {
                // TODO: make this random, and make sure each layout is valid via connectors
                ZoneTileLayout selectedZoneLayoutDefinition = AllLayouts[0];
                // create a new Platforms array with updated Platform positions accounting for the tiles placement in the zone
                List<CollisionRectangle> updatedPlatforms = new List<CollisionRectangle>();
                foreach (CollisionRectangle collRect in selectedZoneLayoutDefinition.Platforms)
                {
                    updatedPlatforms.Add(
                        new CollisionRectangle(
                            collRect.Rect.X + (i * Game1.ZoneTileWidth),
                            collRect.Rect.Y,
                            collRect.Rect.Width,
                            collRect.Rect.Height)
                    );
                }
                // create any foreground animations based on the platform positions
                List<AnimatedSprite> foregroundSprites = createForegroundAnimations(selectedZoneLayoutDefinition.Platforms);
                // create new Nodes array with updated Node positoins accounting for the tiles placement in the zone
                List<Node> nodes = createNodes(selectedZoneLayoutDefinition.NodeLocations, i);
                // create the new layout with updated platforms and node positions
                ZoneTileLayout placedZoneLayout = new ZoneTileLayout(
                    updatedPlatforms,
                    selectedZoneLayoutDefinition.NodeLocations,
                    selectedZoneLayoutDefinition.StartingPositionX,
                    selectedZoneLayoutDefinition.StartingPositionY,
                    foregroundSprites,
                    nodes);

                // save layouts and textures for full zone rendering
                Layouts.Add(placedZoneLayout);
                Textures.Add(AllTextures[0]);
            }
        }

        private void buildMiddleground(int zoneSize)
        {
            // randomly place middleground pieces based on zone size
            // TODO: optimize these to only draw the ones on screen
            Random mgRandom = new Random();
            int mgPlacementX = 0;
            for (int i = 0; i < zoneSize * 3; ++i)
            {
                // select a source rect at random from the middleground texture
                Rectangle mgSourceRect = middlegroundItems[mgRandom.Next(0, middlegroundItems.Count)];
                // add an arbitrary amount to its x position
                int addToX = mgRandom.Next(30, 200);
                // save the source rect to the zone
                MiddlegroundItemSourceRects.Add(mgSourceRect);
                // save the item positions to the zone
                MiddleGroundItemPositions.Add(mgPlacementX + addToX);
                // add the new x position to the x placement counter
                mgPlacementX += addToX;
            }
        }

        private List<Node> createNodes(List<CollisionRectangle> locations, int tileIndex)
        {
            List<Node> nodes = new List<Node>();
            // Random rand = new Random();
            for(int i = 0; i < locations.Count; i++)
            {
                // create testing texture
                Texture2D nodeTexture = new Texture2D(testSpriteBatch.GraphicsDevice, locations[i].Rect.Width, locations[i].Rect.Height);
                Color[] data = new Color[locations[i].Rect.Width * locations[i].Rect.Height];
                for (int j = 0; j < data.Length; ++j) data[j] = Color.White;
                nodeTexture.SetData(data);
                // TODO: create a random check to see if a node should appear at each location
                // create a new rectangle for the node hit checks
                Rectangle hitRect = new Rectangle(
                    locations[i].Rect.X + (tileIndex * Game1.ZoneTileWidth),
                    locations[i].Rect.Y,
                    locations[i].Rect.Width,
                    locations[i].Rect.Height);
                // create the node
                Node node = new Node(
                    locations[i].Rect,
                    hitRect,
                    locations[i].Blocking,
                    nodeTexture);
                // add it to the layout node list
                nodes.Add(node);
            }
            return nodes;
        }

        private List<AnimatedSprite> createForegroundAnimations(List<CollisionRectangle> platforms)
        {
            // create the foreground animations and place them randomly on the platforms
            List<AnimatedSprite> sprites = new List<AnimatedSprite>();
            Random rand = new Random();
            for (int i = 0; i < 20; i++)
            {
                // select a platform at random to place each animation
                CollisionRectangle cr = platforms[rand.Next(platforms.Count)];
                // select a foreground animation at random
                // TODO: make this select at random from a list
                List<Rectangle> animation = ForestForegroundFrames.Sprig1;
                // position each animated sprite based on the platform xy and size
                int sx = cr.Rect.X + rand.Next(cr.Rect.Width - animation[0].Width);
                int sy = cr.Rect.Y - animation[0].Height;
                // create a new sprite
                AnimatedSprite s = new AnimatedSprite(ForegroundTexture, animation, sx, sy);
                sprites.Add(s);
            }
            return sprites;
        }

        private void updateForegroundSprites(GameTime gameTime, List<AnimatedSprite> sprites, Rectangle layoutRect)
        {
            int spriteCount = sprites.Count;
            for (int i = 0; i < spriteCount; i++)
            {
                sprites[i].Update(gameTime, layoutRect.X, layoutRect.Y);
            }
        }

        private void updateNodes(
            GameTime gameTime,
            Input input,
            Character character,
            List<Node> nodes,
            Rectangle layoutRect)
        {
            // loop through each node in the layout
            int numberOfNodes = nodes.Count;
            for(int i = 0; i < numberOfNodes; i++)
            {
                // translate character position to zone tile position
                Rectangle charBounds = new Rectangle((int)character.Position.X, (int)character.Position.Y, character.HitWidth, character.HitHeight);
                // check if the character is hitting it
                Rectangle intersect = Rectangle.Intersect(charBounds, nodes[i].HitRect);
                bool hittingNode = false;
                if (intersect.Width > 0 && intersect.Height > 0) hittingNode = true;
                // if hitting the node, check if the player has activated it
                if (hittingNode && input.Action2Pressed && !input.WasAction2Pressed)
                {
                    // player activated node
                    ActivateNode(nodes[i]);
                }
                nodes[i].Update(gameTime, layoutRect.X, layoutRect.Y, hittingNode);
            }
        }

        public void Update(
            GameTime gameTime,
            Input input,
            Character character)
        {
            VisibleZoneLayouts.Clear();
            VisibleZoneTextures.Clear();

            // get the index of the zone tile the character is most on
            int mainTileIndex = (int)Math.Floor((character.Position.X / Game1.ZoneTileWidth));
            int neighborTileIndex = character.Position.X - (mainTileIndex * Game1.ZoneTileWidth) < Game1.ZoneTileWidth / 2 ? mainTileIndex - 1 : mainTileIndex + 1;
            // offset (left or right) of neighbor tile
            int offset = neighborTileIndex < mainTileIndex ? -Game1.ZoneTileWidth : Game1.ZoneTileWidth;

            // use that to determine the left, middle, and right tile layouts
            VisibleZoneLayouts.Add(Layouts[mainTileIndex]);
            VisibleZoneLayouts.Add(Layouts[neighborTileIndex]);
            // same for corresponding textures
            VisibleZoneTextures.Add(Textures[mainTileIndex]);
            VisibleZoneTextures.Add(Textures[neighborTileIndex]);

            // real tile positions before scaling
            int mainTileX = (int)(character.HitPosition.X - (character.Position.X - (mainTileIndex * Game1.ZoneTileWidth)));
            int neighborTileX = (int)((character.HitPosition.X - (character.Position.X - (mainTileIndex * Game1.ZoneTileWidth))) + offset);
            int bothTileY = (int)(character.HitPosition.Y - character.Position.Y);

            // position the relavent zone tiles based on character position
            // main tile X position
            mainTileDestRect.X = mainTileX;
            // neighbor tile X position
            neighborTileDestRect.X = neighborTileX;
            // both tiles get the same Y position
            mainTileDestRect.Y = neighborTileDestRect.Y = bothTileY;

            // under textures
            mainUnderDestRect.X = mainTileDestRect.X;
            mainUnderDestRect.Y = mainTileDestRect.Y + mainTileDestRect.Height;
            neighborUnderDestRect.X = neighborTileDestRect.X;
            neighborUnderDestRect.Y = neighborTileDestRect.Y + neighborTileDestRect.Height;

            // backgrounds
            mainBackgroundDestRect.X = ((int)(character.Position.X) - (Game1.ZoneTileWidth)) / -8;
            if (mainBackgroundDestRect.X < -Game1.ZoneTileWidth) mainBackgroundDestRect.X = 0;
            neighborBackgroundDestRect.X = mainBackgroundDestRect.X + (Game1.ZoneTileWidth);

            // redo middleground rects
            middlegroundDestRects.Clear();
            int mgItems = MiddlegroundItemSourceRects.Count;
            for (int i = 0; i < mgItems; ++i)
            {
                Rectangle mgDestRect = new Rectangle(
                    (MiddleGroundItemPositions[i]) - (int)character.Position.X / 3,
                    (int)(character.Position.Y + 30) / -4,
                    MiddlegroundItemSourceRects[i].Width,
                    MiddlegroundItemSourceRects[i].Height);
                middlegroundDestRects.Add(mgDestRect);
            }

            // update the animated foreground sprites
            updateForegroundSprites(gameTime, VisibleZoneLayouts[0].Sprites, mainTileDestRect);
            updateForegroundSprites(gameTime, VisibleZoneLayouts[1].Sprites, neighborTileDestRect);

            // update the node locations
            updateNodes(gameTime, input, character, VisibleZoneLayouts[0].Nodes, mainTileDestRect);
            updateNodes(gameTime, input, character, VisibleZoneLayouts[1].Nodes, neighborTileDestRect);
        }

        public void DrawBackground(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(BackgroundTexture, mainBackgroundDestRect, Color.White);
            spriteBatch.Draw(BackgroundTexture, neighborBackgroundDestRect, Color.White);
        }

        public void DrawMiddleground(SpriteBatch spriteBatch)
        {
            int mgItems = MiddlegroundItemSourceRects.Count;
            for (int i = 0; i < mgItems; ++i)
            {
                spriteBatch.Draw(
                    texture: MiddlegroundTexture,
                    destinationRectangle: middlegroundDestRects[i],
                    sourceRectangle: MiddlegroundItemSourceRects[i],
                    color: Color.White
                );
            }
        }

        public void DrawPlatforms(SpriteBatch spriteBatch)
        {
            // always add the 2 relavent zone textures
            spriteBatch.Draw(VisibleZoneTextures[0], mainTileDestRect, Color.White);
            spriteBatch.Draw(VisibleZoneTextures[1], neighborTileDestRect, Color.White);
            // and the zone type underground aligns with them
            spriteBatch.Draw(UnderTexture, mainUnderDestRect, Color.White);
            spriteBatch.Draw(UnderTexture, neighborUnderDestRect, Color.White);
        }

        public void DrawForeground(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < 2; i++)
            {
                // update each animated sprite in each tile with new position info
                int sprites = VisibleZoneLayouts[i].Sprites.Count;
                for (int j = 0; j < sprites; j++)
                {
                    VisibleZoneLayouts[i].Sprites[j].Draw(spriteBatch);
                }
            }
        }

        public void DrawNodes(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < 2; i++)
            {
                // update each animated sprite in each tile with new position info
                int nodes = VisibleZoneLayouts[i].Nodes.Count;
                for (int j = 0; j < nodes; j++)
                {
                    VisibleZoneLayouts[i].Nodes[j].Draw(spriteBatch);
                }
            }
        }

        public void DestroyZone()
        {

        }
    }
}
