﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using Microsoft.Xna.Framework;

namespace DaggersLast
{
    public static class ZoneTileData
    {
        public static ZoneTileLayout Tile5a;
        public static void Generate()
        {
            // there should be one tile object for each tile layout (20-30 total)
            // zone type graphics and node options are defined elsewhere and applied at zone creation
            // add the platforms
            Tile5a = new ZoneTileLayout(
                new List<CollisionRectangle>()
                {
                    new CollisionRectangle(x: 0, y: 40, width: 20, height: 20),
                    new CollisionRectangle(x: 160, y: 40, width: 60, height: 20),
                    new CollisionRectangle(x: 200, y: 60, width: 100, height: 20),
                    new CollisionRectangle(x: 20, y: 60, width: 20, height: 20),
                    new CollisionRectangle(x: 60, y: 60, width: 20, height: 20),
                    new CollisionRectangle(x: 300, y: 180, width: 20, height: 20),
                    new CollisionRectangle(x: 300, y: 40, width: 20, height: 20),
                    new CollisionRectangle(x: 20, y: 80, width: 100, height: 20),
                    new CollisionRectangle(x: 80, y: 100, width: 80, height: 20),
                    new CollisionRectangle(x: 120, y: 160, width: 120, height: 20),
                    new CollisionRectangle(x: 200, y: 140, width: 80, height: 20),
                    new CollisionRectangle(x: 80, y: 180, width: 60, height: 20),
                    new CollisionRectangle(x: 20, y: 260, width: 40, height: 20),
                    new CollisionRectangle(x: 180, y: 340, width: 40, height: 20),
                    new CollisionRectangle(x: 260, y: 340, width: 60, height: 20),
                    new CollisionRectangle(x: 40, y: 280, width: 60, height: 20),
                    new CollisionRectangle(x: 0, y: 340, width: 120, height: 20),
                    new CollisionRectangle(x: 120, y: 320, width: 160, height: 20),
                    new CollisionRectangle(x: 140, y: 260, width: 100, height: 20),
                    new CollisionRectangle(x: 220, y: 240, width: 80, height: 20),
                    new CollisionRectangle(x: 260, y: 220, width: 40, height: 20),
                    new CollisionRectangle(x: 280, y: 200, width: 40, height: 20),
                },
                // add the nodes
                // these could be encounters, collectables, npcs, or (maybe) instant effects (like damage, or a malady)
                new List<CollisionRectangle>()
                {
                    new CollisionRectangle(x: 160, y: 140, width: 40, height: 20, isEncounter: true, isBlocking: true),
                },
                // starting character x position (if this tile is the landing tile from a town)
                33,
                // starting character y position
                314
            );
        }
    }
}