﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace DaggersLast
{
    public class ZoneTileLayout
    {
        public readonly List<CollisionRectangle> Platforms;
        public readonly List<CollisionRectangle> NodeLocations;
        public readonly int StartingPositionY;
        public readonly int StartingPositionX;
        public readonly List<AnimatedSprite> Sprites;
        public readonly List<Node> Nodes;

        public ZoneTileLayout(
            List<CollisionRectangle> platforms,
            List<CollisionRectangle> nodeLocations,
            int startingPositionX,
            int startingPositionY,
            List<AnimatedSprite> sprites = null,
            List<Node> nodes = null)
        {
            Platforms = platforms;
            NodeLocations = nodeLocations;
            Nodes = nodes;
            StartingPositionX = startingPositionX;
            StartingPositionY = startingPositionY;
            if (sprites != null) Sprites = sprites;
            if (nodes != null) Nodes = nodes;
        }
    }
}
