﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace DaggersLast
{
    public class CollisionRectangle
    {
        public Rectangle Rect;
        public bool Encounter;
        public bool Blocking;

        public CollisionRectangle(int x, int y, int width, int height, bool isEncounter = false, bool isBlocking = false)
        {
            Rect = new Rectangle(x, y, width, height);
            Encounter = isEncounter;
            Blocking = isBlocking;
        }
    }
}
